
const { User } = require("../database/models");

const authUser =  async ({ email }) => User.findOne({ where: { email }});

const functionAuthRepo = {
    authUser
}
module.exports = functionAuthRepo;
