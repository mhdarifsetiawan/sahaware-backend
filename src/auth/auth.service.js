const authRepo = require("./auth.repo");

const authUser =  async ({ email }) => authRepo.authUser({ email })

const functionAuthService = {
    authUser,
}

module.exports = functionAuthService;