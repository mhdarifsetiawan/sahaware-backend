const express = require("express");
const { checkSchema } = require("express-validator");
const tokenVerification = require("../middleware/token.verification");
const {
  registrationValidationObject,
} = require("../middleware/user.validation");
const { validate } = require("../middleware/validation");
const userRouter = express.Router();

const userController = require("./user.controller");

userRouter.get("/users", userController.getAllUser);

// API USER REGISTRATION
/**
 * @swagger
 * /user/registration:
 *  post:
 *      tags:
 *          - users
 *      summary: API user registration (PUBLIC &VALIDATION)
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      type: object
 *                      properties:
 *                          fullname:
 *                              type: string
 *                              example: Arif Setiawan
 *                          email:
 *                              type: string
 *                              example: arif@gmail.com
 *                          password:
 *                              type: string
 *                              example: Password@123!
 *      responses:
 *          '200':
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              id:
 *                                  type: string
 *                              fullname:
 *                                  type: string
 *                              email:
 *                                  type: string
 *                              password:
 *                                  type: string
 *                              createdAt:
 *                                  type: string
 *                              updatedAt:
 *                                  type: string
 */
userRouter.post(
  "/user/registration",
  checkSchema(registrationValidationObject),
  validate,
  userController.createUser
);

module.exports = userRouter;
