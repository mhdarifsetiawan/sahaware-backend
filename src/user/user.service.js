const bcrypt = require("bcrypt");
const userRepo = require("./user.repo");
const saltRound = 10;

const generator = require("generate-password");

const getAllUser = async ({ pageNumber, limitParm }) => {
  const getAllUserinRepo = await userRepo.getAllUser({ pageNumber, limitParm });
  return getAllUserinRepo;
};

const createUser = async ({ fullname, email, password }) => {
  const hashPassword = await bcrypt.hash(password, saltRound);
  // console.log(hashPassword);
  const checkEmailUser = await userRepo.checkEmailAllUser(email);
  // console.log(checkEmailUser);

  if (!checkEmailUser) {
    const getUserRepo = await userRepo.createUser({
      fullname,
      email,
      password: hashPassword,
    });
    // console.log(getUserRepo);
    return getUserRepo;
  } else return null;
};

const findEmail = async ({ email }) => {
  const checkEmailUser = await userRepo.checkEmailAllUser(email);
  // const userId = checkEmailUser.id;
  // const userName = checkEmailUser.fullname;
  if (checkEmailUser) {
    const userId = checkEmailUser.id;
    const userName = checkEmailUser.fullname;
    //generate password
    const password = generator.generate({
      length: 10,
      numbers: true,
      symbols: true,
    });
  } else {
    return null;
  }
  return checkEmailUser.email;
};

const getUserById = async ({ userId }) => {
  return await userRepo.getUserById({ userId });
};

const userService = {
  createUser,
  getAllUser,
  getUserById,
  findEmail,
};

module.exports = userService;
