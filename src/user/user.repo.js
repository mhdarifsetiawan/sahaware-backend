const { User } = require("../database/models");

let pageFormula = 8 + 1 - 1;
let limitValue = 8;

const getAllUser = async ({ pageNumber, limitParm }) => {
  if (limitParm != undefined) limitValue = limitParm;
  if (pageNumber != undefined) {
    return await User.findAll({
      order: [["score", "DESC"]],
      offset: (pageNumber - 1) * pageFormula,
      limit: limitValue,
      attributes: ["id", "fullname", "score"],
    });
  } else {
    return await User.findAll();
  }
};

const createUser = async ({ fullname, email, password }) => {
  return await User.create({
    fullname,
    email,
    password,
  });
};

const checkEmailAllUser = async (email) => {
  return await User.findOne({
    where: { email: email },
  });
};

const checkSameEmail = async ({ email, authUserId }) => {
  return await User.findOne({
    where: {
      email: email,
      id: authUserId,
    },
  });
};

const getUserById = async ({ userId }) => {
  return await User.findOne({
    where: {
      id: userId,
    },
  });
};

const userRepo = {
  createUser,
  getAllUser,
  checkEmailAllUser,
  checkSameEmail,
  getUserById,
};

module.exports = userRepo;
